import { createApp } from "vue";
import { createI18n, I18nOptions } from "vue-i18n";
import App from "./App.vue";
import "bootstrap/dist/css/bootstrap.min.css";
import messages from "./messages.json";
import datetimeFormats from "./datetimeFormats.json";
const i18n = createI18n({
  allowComposition: true, // you need to specify that!
  locale: navigator.language.split("-")[0],
  fallbackLocale: "en",
  silentFallbackWarn: true,
  silentTranslationWarn: true,
  formatFallbackMessages: true,
  messages,
  datetimeFormats: datetimeFormats as I18nOptions["datetimeFormats"],
});

const app = createApp(App);
app.use(i18n);
app.mount("#app");
