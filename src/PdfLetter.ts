interface Letter {
  senderName: string;
  senderAddress: string;
  recipientName: string;
  recipientAddress: string;
  greeting: string;
  date: string;
  body: string;
  closing: string;
}

function findXToAlignRight(pdf: PDFKit.PDFDocument, textWidth: number): number {
  return pdf.page.width - pdf.page.margins.right - textWidth - 1;
}

function findRecipientBlockWidth(
  pdf: PDFKit.PDFDocument,
  letter: Letter
): number {
  let maxWidth = pdf.widthOfString(letter.recipientName);
  const lines = letter.recipientAddress.split("\n");
  for (let i = 0; i < lines.length; i++) {
    const lineWidth = pdf.widthOfString(lines[i]);
    if (lineWidth > maxWidth) {
      maxWidth = lineWidth;
    }
  }
  return maxWidth;
}

export function fillPdfLetter(pdf: PDFKit.PDFDocument, letter: Letter): void {
  pdf.info.Author = letter.senderName;
  pdf.fontSize(14);
  pdf.text(letter.senderName, pdf.page.margins.left, pdf.page.margins.top + 20);
  pdf.text(letter.senderAddress);
  const recipientX = findXToAlignRight(
    pdf,
    findRecipientBlockWidth(pdf, letter)
  );
  pdf.text(letter.recipientName, recipientX, pdf.page.margins.top);
  pdf.text(letter.recipientAddress);
  pdf.moveDown(5);
  pdf.text(letter.date, findXToAlignRight(pdf, pdf.widthOfString(letter.date)));
  pdf.moveDown(2);
  const indent = 20;
  pdf.text(letter.greeting, pdf.page.margins.left + indent);
  pdf.moveDown();
  pdf.text(letter.body, pdf.page.margins.left, undefined, {
    align: "justify",
    indent,
  });
  pdf.moveDown();
  pdf.text(letter.closing, pdf.page.margins.left + indent);
  pdf.moveDown();
  pdf.text(letter.senderName);
}
