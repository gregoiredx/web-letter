import fs from "fs";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function registerAFMFonts(ctx: any): void {
  ctx.keys().forEach((key: string) => {
    const match = key.match(/([^/]*\.afm$)/);
    if (match) {
      // afm files must be stored on data path
      fs.writeFileSync(`data/${match[0]}`, ctx(key).default);
    }
  });
}

export function register(): void {
  // register AFM fonts distributed with pdfkit
  // is good practice to register only required fonts to avoid the bundle size increase
  registerAFMFonts(
    require.context("pdfkit/js/data", false, /Helvetica.*\.afm$/)
  );
}
