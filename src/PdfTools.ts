import blobStream, { IBlobStream } from "blob-stream";
// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const pdffonts = () => import(/* webpackChunkName: "pdffonts" */ "./PdfFonts");
// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const pdfkit = () => import(/* webpackChunkName: "pdfkit" */ "pdfkit");

type PdfFiller = (doc: PDFKit.PDFDocument) => void;

async function newPdfDocument(): Promise<PDFKit.PDFDocument> {
  const [fontsModule, pdfkitModule] = await Promise.all([pdffonts(), pdfkit()]);
  fontsModule.register();
  const importedPdfDocumentClass = pdfkitModule.default;
  return new importedPdfDocumentClass({ size: "A4" });
}

async function createPdf(fillPdf: PdfFiller): Promise<PDFKit.PDFDocument> {
  const doc = await newPdfDocument();
  fillPdf(doc);
  doc.end();
  return doc;
}

function pdfDocToStream(doc: PDFKit.PDFDocument): Promise<IBlobStream> {
  return new Promise((resolve) => {
    const stream = doc.pipe(blobStream());
    stream.on("finish", () => {
      resolve(stream);
    });
  });
}
export async function buildPdfURL(fillPdf: PdfFiller): Promise<string> {
  const stream = await pdfDocToStream(await createPdf(fillPdf));
  return stream.toBlobURL("application/pdf");
}
